#include <iostream>

const int ROWS = 3;
const int COLS = 3;
const int SIZE = COLS * ROWS;

// Colors per field are Empty, X, O
enum Color { E, X, O };


int getMove(Color c);
void setMove(Color (*board)[SIZE], Color c, int i);

bool isValid(Color board[SIZE], int i);
bool isWinner(Color board[SIZE], Color c);
bool isWinnerInRow(Color board[SIZE], Color c, int i);
bool isWinnerInCol(Color board[SIZE], Color c, int i);
bool isWinnerInDia(Color board[SIZE], Color c, bool inverse);

void printBoard(Color board[SIZE]);
void printReference();



int main() {
    Color board[SIZE] { E, E, E,
                        E, E, E,
                        E, E, E };

    Color winner = E;
    int turns = 0;

    do {
        Color player = turns % 2 == 0 ? X : O;

        printBoard(board);
        int move = getMove(player);

        while(!isValid(board, move)) {
            std::cout << "Invalid move! Please try again." << std::endl;
            printBoard(board);
            move = getMove(player);
        }

        setMove(&board, player, move);

        // determine winner
        if (isWinner(board, X))
            winner = X;
        else if (isWinner(board, O))
            winner = O;
    } while (winner == E && (++turns < SIZE));

    printBoard(board);

    if (winner == E)
        std::cout << "It's a tie!" << std::endl;
    else
        std::cout << "The winner is player " << (winner == X ? "x" : "o") << std::endl;

    return 0;
}

int getMove(Color c) {

    std::cout << "Turn of player ";
    if (c == X)
        std::cout << "x";
    else if (c == O)
        std::cout << "o";
    std::cout << std::endl;

    char choice;
    do {
        std::cout << std::endl;
        printReference();
        std::cout << "Please enter a field index: ";
        std::cin >> choice;
    } while(choice < '1' || choice >= '1' + SIZE);

    choice -= '1';
    return (int) choice;
}

void setMove(Color (*board)[SIZE], Color c, int i) {
    (*board)[i] = c;
}

bool isValid(Color board[SIZE], int i) {
    return board[i] == E;
}

bool isWinner(Color board[SIZE], Color c) {
    bool isWinner = false;
    for (int i = 0; i < ROWS; i ++) {
        isWinner = isWinner || isWinnerInRow(board, c, i) || isWinnerInCol(board, c, i);
    }
    isWinner = isWinner || isWinnerInDia(board, c, false) || isWinnerInDia(board, c, true);
    return isWinner;
}

bool isWinnerInRow(Color board[SIZE], Color c, int i) {
    i *= ROWS;
    return c == board[i] && c == board[i + 1] && c == board[i + 2];
}

bool isWinnerInCol(Color board[SIZE], Color c, int i) {
    i %= COLS;
    return c == board[i] && c == board[i + COLS] && c == board[i + 2 * COLS];
}

bool isWinnerInDia(Color board[SIZE], Color c, bool inverse) {
    if (inverse) {
        return c == board[2] && c == board[4] && c == board[6];
    } else {
        return c == board[0] && c == board[4] && c == board[8];
    }
}

void printBoard(Color board[SIZE]) {

    std::cout << "== Board ==" << std::endl;

    for (int i = 0 ; i < SIZE; i++) {
        char c;
        switch (board[i]) {
            case E:
                c = ' ';
                break;
            case X:
                c = 'x';
                break;
            case O:
                c = 'o';
                break;
        }

        std::cout << " " << c << " ";

        // add column seperator: '|'
        if (i % COLS != 2) {
            std::cout << "|";
        } else {
            // add row seperator: '\n'
            std::cout << std::endl;
            if (i < SIZE - 1)
                // Row seperator not for last row
                std::cout << "---+---+---" << std::endl;
        }
    }

    std::cout << std::endl;
}

void printReference() {
    std::cout << "=== Ref ===" << std::endl;
    std::cout << "_1_|_2_|_3_" << std::endl;
    std::cout << "_4_|_5_|_6_" << std::endl;
    std::cout << " 7 | 8 | 9 " << std::endl;
}

